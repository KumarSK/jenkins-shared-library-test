def call(){
    script{
                    withSonarQubeEnv('sonar') {
                        echo "sonar scan has initiated"
                        sh'mvn --version'
                        sh'mvn sonar:sonar'
                        echo "sonar scan has completed"
                    }
                }
}
